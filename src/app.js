import './styles/index.less';

import $ from 'jquery';

import { Person } from './js/main';

function fetchData(callback) {
  const URL = 'http://localhost:3000/person';
  fetch(URL)
    .then(response => response.json())
    .then(json => {
      const person = new Person(
        json.name,
        json.age,
        json.description,
        json.educations
      );
      callback(person);
    });
}

function renderHtml(person) {
  $('.user-description').html(person.description);
  $('.user-name').html(person.name);
  $('.user-age').html(person.age);

  const educations = person.educations;
  for (let i = 0; i < educations.length; i++) {
    $('#experience').append(
      `<li><h4>${educations[i].year}</h4><section><h5>${educations[i].title}</h5><p>${educations[i].description}</p></section></li>`
    );
  }
}

$(function() {
  fetchData(renderHtml);
});
