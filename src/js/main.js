export class Person {
  constructor(name, age, description, educations) {
    this._name = name;
    this._age = age;
    this._description = description;
    this._educations = educations.map(
      education =>
        new Education(education.year, education.title, education.description)
    );
  }

  get educations() {
    return this._educations;
  }

  get name() {
    return this._name;
  }

  get age() {
    return this._age;
  }

  get description() {
    return this._description;
  }
}

export class Education {
  constructor(year, title, description) {
    this._year = year;
    this._title = title;
    this._description = description;
  }

  get year() {
    return this._year;
  }

  get title() {
    return this._title;
  }

  get description() {
    return this._description;
  }
}
